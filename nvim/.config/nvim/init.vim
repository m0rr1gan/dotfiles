set nocompatible              " be iMproved, required
"filetype off                  " required
"filetype plugin indent on

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""""""" Plugin Installation""""""""""""""""""""""""""""""""
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Specify a directory for plugins
" - For Neovim: ~/.local/share/nvim/plugged
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.config/nvim/plugged')

" Plugins after this line

"might be able to take a lot out because of this 
Plug 'tpope/vim-sensible'
" automatically adjust tabs based on filetype
Plug 'tpope/vim-sleuth'
"super nice mappings, read about them 
Plug 'tpope/vim-unimpaired'
Plug 'tpope/vim-surround'


"NERD tree plugin
Plug 'scrooloose/nerdtree'

"async syntax checker vs syntastic
Plug 'w0rp/ale'

"easymotion plugin
Plug 'easymotion/vim-easymotion'

"align text with simple bindings (no regex)
Plug 'junegunn/vim-easy-align'

"Airline plugin (status bar/ tabline)
Plug 'vim-airline/vim-airline'

"column for git revisions and navigation
Plug 'airblade/vim-gitgutter'

"NERD commenter plugin (commenting ease)
Plug 'scrooloose/nerdcommenter'

"fuzzy finder 
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'

" go support
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }

" adds more matching for %
Plug 'andymass/vim-matchup'

"vimtex - plugin for editing LaTeX files
Plug 'lervag/vimtex'

"vim-table - make tables easily in vim
Plug 'dhruvasagar/vim-table-mode'

"""" Unused Plugins '"""""""""""""""""""""""""""""""""""""""
"look into the ctrlspace plugin, might be good for workspace management

"Fugitive git wrapper plugin
"Plug 'tpope/vim-fugitive'
"Plug 'tpope/vim-sleuth' "?

"multiple cursors in vim
"Plug 'terryma/vim-multiple-cursors'

"Syntastic syntax checking plugin *not needed with YCM
"Plug 'scrooloose/syntastic'

"line text up easily
"Plug 'godlygeek/tabular'

"Plug 'vim-airline/vim-airline-themes'

"ctrlp finder plugin *old version
"Plug 'kien/ctrlp.vim'

"Solarized color scheme for Vim
"Plug 'altercation/vim-colors-solarized'

"code completion engine for vim
"Plug 'valloric/youcompleteme'

"new ctlp fuzzy finder etc
"Plug 'ctrlpvim/ctrlp.vim'

"JavaScipt plugin
"Plug 'pangloss/vim-javascript'

"Vim Hard Mode
"Plug 'wikitopian/hardmode'

"lightweight tab completion for vim
"Plug 'ervandew/supertab'

"Tagbar for vim (ordered by scope)
"Plug 'majutsushi/tagbar'

"automaticaly manage ctags
"Plug 'ludovicchabant/vim-gutentags'

" auto insert closing quotes, parens etc
" Plug 'raimondi/delimitmate'

call plug#end()
"""""""""""""""""""""""""""""""""""""""""""""""""""
""""""""" Base Vim formatting '""""""""""""""""""""
"""""""""""""""""""""""""""""""""""""""""""""""""""

"use both relative number and number by default
set relativenumber
set number

"enables search highighting and matches as the characters
"as they are typed and ignores case unless all caps (in order)
set hlsearch
set ignorecase
set smartcase
"yoh to toggle highlighting

"use spell checking by default
"set spell spelllang=en_us
set spelllang=en_us
"yos to toggle spelling

"set the default tab to 4 spaces and use spaces instead of one a tab
"set shiftwidth=4 softtabstop=4 expandtab "tabstop=4 is irrelevant (reddit)

"set the length of the text on a line to auto wrap
"set textwidth=78
set wrap
"to reformat a file gg -> gq -> G

"set ignore case for wildmenu
set wildignore+=*/tmp/*,*.so,*.swp,*.zip

"show partial commands in the lowest line
set showcmd

"allow backspacing over autoindent
set backspace=indent,eol,start

"keep indent same as line above when without file extension
set autoindent

"raise dialogue on failed command asking if save is wanted
set confirm

"set command window height to 2 lines
"set cmdheight=2

"Makes the color for the airline correct
let &t_Co=256

"<leader> character used to initiate certain keybinds
let mapleader=" "

"allows buffers to be hidden if its modified
set hidden

"open a new empty buffer
nmap <leader>n :enew<CR>

"close current buffer and move to the previous one
nmap <leader>d :bp <BAR> bd #<CR>

"show all open buffers and their status
nmap <leader>l :ls<CR>

"[b and ]b can be used to go to prev and next
"[B and ]B for first and last

"easier buffer switching by number
nnoremap <Leader>1 :1b<CR>
nnoremap <Leader>2 :2b<CR>
nnoremap <Leader>3 :3b<CR>
nnoremap <Leader>4 :4b<CR>
nnoremap <Leader>5 :5b<CR>
nnoremap <Leader>6 :6b<CR>
nnoremap <Leader>7 :7b<CR>
nnoremap <Leader>8 :8b<CR>
nnoremap <Leader>9 :9b<CR>
nnoremap <Leader>0 :10b<CR>

"easier movement between splits
"nnoremap <C-J> <C-W><C-J>
"nnoremap <C-K> <C-W><C-K>
"nnoremap <C-L> <C-W><C-L>
"nnoremap <C-H> <C-W><C-H>

"more natural split opening
set splitbelow
set splitright

"disable arrow keys and page up/down entirely in every mode
nnoremap <Left> <nop>
nnoremap <Right> <nop>
nnoremap <Up> <nop>
nnoremap <Down> <nop>
nnoremap <PageUp> <nop>
nnoremap <PageDown> <nop>

vnoremap <Left> <nop>
vnoremap <Right> <nop>
vnoremap <Up> <nop>
vnoremap <Down> <nop>
vnoremap <PageUp> <nop>
vnoremap <PageDown> <nop>

inoremap <Left> <nop>
inoremap <Right> <nop>
inoremap <Up> <nop>
inoremap <Down> <nop>
inoremap <PageUp> <nop>
inoremap <PageDown> <nop>

"""""""""""""""""""""""""""""""""""""""""""""""""""
""""""""""""""" Plugin configuration '"""""""""""""
"""""""""""""""""""""""""""""""""""""""""""""""""""
"shortcut for nerdtree
nmap <Leader>t :NERDTreeToggle<CR>

" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap <Leader>a <Plug>(EasyAlign)

" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap <Leader>a <Plug>(EasyAlign)

" air-line powerline fonts enabling
"let g:airline_powerline_fonts = 0

" show errors and warnings in statusline
let g:airline#extensions#ale#enabled = 1
"move between errors in ale
nmap <silent> <Leader>k <Plug>(ale_previous_wrap)
nmap <silent> <Leader>j <Plug>(ale_next_wrap)
"show 5 lines of errors, default is 10
let g:ale_list_window_size = 5

"don't highlight anything (still has the sidebar >>)
let g:ale_set_highlights = 0
highlight ALEWarning ctermbg=DarkMagenta
highlight ALEError ctermbg=DarkMagenta

"run ale on file save (needed for linters that only work on saved files as
"opposed to tmp files too)
let g:ale_lint_on_save = 1

" easymotion settings ''''''''''''''''''''''''''''''''''''''''''''''
" just leader for easymotion prefix
map <Leader> <Plug>(easymotion-prefix)
let g:EasyMotion_do_mapping = 0 " Disable default mappings

" Jump to anywhere you want with minimal keystrokes, with just one key binding.
" `s{char}{label}`
nmap <Leader>s <Plug>(easymotion-overwin-f)
" or
" `s{char}{char}{label}`
" Need one more keystroke, but on average, it may be more comfortable.
nmap <Leader>s <Plug>(easymotion-overwin-f2)

" Turn on case-insensitive feature
let g:EasyMotion_smartcase = 1

" JK motions: Line motions
map <Leader>j <Plug>(easymotion-j)
map <Leader>k <Plug>(easymotion-k)

" fzf file and inside file search
nnoremap <Leader>f :Files<CR>
nnoremap <Leader>g :Rg<CR>

"vimtex needed setting
let g:tex_flavor = 'latex'
let g:vimtex_compiler_progname = 'nvr'
let g:vimtex_view_method = 'general'
let g:vimtex_view_general_viewer = 'mupdf'
let g:vimtex_quickfix_open_on_warning = 0
"nvr --remote-silent %f -c %l
"''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

"enable list of buffers
"let g:airline#extensions#tabline#enabled = 1
"show the filename in the buffer list
"let g:airline#extensions#tabline#fnamemod = ':t'
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#tab_nr_type = 1 " tab number
"let g:airline#extensions#tabline#show_tab_nr = 1
"let g:airline#extensions#tabline#formatter = 'default'
let g:airline#extensions#tabline#buffer_nr_show = 1
"let g:airline#extensions#tabline#fnametruncate = 16
"let g:airline#extensions#tabline#fnamecollapse = 2
"let g:airline#extensions#tabline#buffer_idx_mode = 1

if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif

" unicode symbols
let g:airline_left_sep = '»'
let g:airline_left_sep = '▶'
let g:airline_right_sep = '«'
let g:airline_right_sep = '◀'
let g:airline_symbols.crypt = '🔒'
let g:airline_symbols.linenr = '␊'
let g:airline_symbols.linenr = '␤'
let g:airline_symbols.linenr = '¶'
let g:airline_symbols.maxlinenr = ''
let g:airline_symbols.branch = '⎇'
let g:airline_symbols.paste = 'ρ'
let g:airline_symbols.paste = 'Þ'
let g:airline_symbols.paste = '∥'
let g:airline_symbols.spell = 'Ꞩ'
let g:airline_symbols.notexists = '∄'
let g:airline_symbols.whitespace = 'Ξ'

"time out on keycodes but never on mappings (also airline)
"set notimeout ttimeout ttimeoutlen=200

"limit size of syntastic list
"function! SyntasticCheckHook(errors)
"    if !empty(a:errors)
"        let g:syntastic_loc_list_height = min([len(a:errors), 10])
"    endif
"endfunction
"
""CtrlP ease of use settings
"let g:ctrlp_match_window = 'bottom,order:ttb'
"let g:ctrlp_switch_buffer = 'et'
"let g:ctrlp_working_path_mode = 'ra'
"
""CtrlP mappings
"let g:ctrlp_map = '<c-p>'
"let g:ctrl_cmd = 'CtrlP'
"
""use ctrlp to search the tags file generated by ctags
"nnoremap <Leader>. :CtrlPTag<cr>
"
""custom file listing
"let g:ctrlp_user_command = 'find %s -type f'
"
""ignore case for CtrlP
"let g:ctrlp_custom_ignore = {
"  \ 'dir':  '\v[\/]\.(git|hg|svn)$',
"  \ 'file': '\v\.(tar|so|dll|o)$',
"  \ 'link': 'some_bad_symbolic_links',
"  \ }
""use the_silver_searcher for ctrlp searches
""let g:ctrlp_user_command = 'ag %s -l --nocolor --hidden -g ""'
"
""Sets the new user settings for syntastic
"set statusline+=%#warningmsg#
"set statusline+=%{SyntasticStatuslineFlag()}
"set statusline+=%*
"
"let g:syntastic_always_populate_loc_list = 1
"let g:syntastic_auto_loc_list = 1
"let g:syntastic_check_on_open = 1
"let g:syntastic_check_on_wq = 0
"
" "Sets the compiler options for syntastic for C++
"let g:syntastic_cpp_compiler_options = '-std=c++11 -Wall -Wextra -Wpedantic'
"
""options for c compilation
"let g:syntastic_c_compiler_options = '-Wall -Werror -Wmissing-prototypes'

"shortcut for tagbar
"nmap <Leader>b :TagbarToggle<CR>

"source the cscope file 
"source ~/.config/nvim/plugins/cscope_maps.vim
