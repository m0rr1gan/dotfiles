# Personal Dotfiles
Use GNU Stow to symlink the config files from their system location to here.

Clone into your $HOME then run stow for whatever configs you want:

```bash
git clone git@gitlab.com:m0rr1gan/dotfiles.git ~/.dotfiles
cd .dotfiles
stow emacs zsh #any config files you want here
```
and replace emacs with all directories to create symlinks for.

Stow makes these symlinks in the parent directory of the CWD so if the project
folder is anywhere other than directly in $HOME, the flag --target=~ should be used.

Emacs will install its plugins automatically on starting it.

### neovim 
Can be used for vim instead with minor changes, namely the directory for the
plugins

This does include vim-plug but not the plugins that get installed by it, run:
```bash
nvim +PlugInstall +qa
```
to install the plugins.

### st
A PKGBUILD for st to be used with makepkg in Arch.

Note: this directory does not need to be called by stow, but you will have to
enter it and run:
```bash
makepkg -sic
```
to build st as well after stow'ing the bin/ directory.

Includes scrollback, hidecursor and externalpipe. externalpipe has two script
dependencies in the /bin directory: xurls and open which are all used for
extracting and opening urls from the terminal through dmenu and these scripts.

### Dependency software
GNU Stow

Fonts: font awesome for status bars and preferred terminal font

Packages for each folder name to be used (emacs, zsh, etc)
