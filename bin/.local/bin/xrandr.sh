#!/bin/bash

# make temp file and set it to be deleted on script end or kill
full_output=$(mktemp)
trap 'rm -f $full_output' 0 2 3 15

#get connected monitors and remove the + that comes in from of the output name
xrandr --listactivemonitors | awk '{print $4}' > $full_output

# process the file to pull out vars
while read line ; do
    if [[ $line = D* ]] ; then
        dp=$line
    elif [[ $line = H* ]] ; then
        hdmi=$line
    fi
done < $full_output

# make sure to remove files
rm -f $full_output

# call the xrandr command to set screen orientation correctly
xrandr --output $dp --rotate normal --auto --left-of $hdmi --output $hdmi --rotate inverted --auto 
