unalias -a

#auto loading widgets
autoload -Uz compinit && compinit
autoload -U edit-command-line
zle -N edit-command-line
autoload -U up-line-or-beginning-search
autoload -U down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search
# git branch info
autoload -Uz add-zsh-hook vcs_info

HISTFILE=$HOME/.zsh_history
HISTSIZE=10000
SAVEHIST=10000

#options
setopt HIST_IGNORE_SPACE
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_ALL_DUPS
setopt EXTENDED_HISTORY
setopt APPEND_HISTORY
setopt INC_APPEND_HISTORY
setopt SHARE_HISTORY
setopt COMPLETE_ALIASES
setopt AUTO_CD
setopt EXTENDED_GLOB
setopt ALWAYS_TO_END
setopt AUTO_MENU
setopt NO_BG_NICE
setopt NO_HUP
setopt NO_BEEP
setopt RM_STAR_SILENT
# needed for $var in prompts
setopt PROMPT_SUBST

#vim mode keybindings

bindkey -v
# Better searching in command mode
bindkey -M vicmd '?' history-incremental-search-backward
bindkey -M vicmd '/' history-incremental-search-forward

# Beginning search with arrow keys
bindkey "^[[A" up-line-or-beginning-search
bindkey "^[[B" down-line-or-beginning-search
bindkey -M vicmd "k" up-line-or-beginning-search
bindkey -M vicmd "j" down-line-or-beginning-search
#open command in vim
bindkey -M vicmd v edit-command-line

#allow termite to set directory 
#if [[ $TERM == xterm-termite ]]; then
#    . /etc/profile.d/vte.sh
#    __vte_osc7
#fi

#exports

export SUDO_EDITOR=/usr/bin/nvim
export EDITOR=/usr/bin/nvim
export VISUAL=/usr/bin/nvim
export TERMINAL=/usr/bin/alacritty
export BROWSER=/usr/bin/firefox
export KEYTIMEOUT=1
export PATH=~/.local/bin:~/go/bin:$PATH
export DIFFPROG='nvim -d'
export BEMENU_BACKEND='wayland'

# # functions

vpn() {
    #countries to use ch, nl, no, ic
    #                 switz, netherlands, norway, iceland(maybe)
    #use shuf command to generate number 350-2600 (careless brute force)
    num=`shuf -i 300-4700 -n 1`
    #default to the us if no arg
    arg=$1
    if [[ ! -n $1 ]]; then
        arg=us
    fi
    
    fp=/home/anon/.nordvpn/ovpn_udp/$arg$num.nordvpn.com.udp.ovpn
    #check for valid file path and try new ones until we get one
    while [[ ! -a $fp ]]
    do
        num=`shuf -i 50-3200 -n 1`
        fp=/home/anon/.nordvpn/ovpn_udp/$arg$num.nordvpn.com.udp.ovpn
    done

    echo $fp
    sudo openvpn $fp
}

pulse() {
    if ( pulseaudio --check )
    then
        pulseaudio --kill
        sleep 1
        pulseaudio --start
    else
        pulseaudio --start
    fi
}

#aliases
alias httpsreflector='sudo reflector --country "United States" --age 12 --protocol https --sort rate --save /etc/pacman.d/mirrorlist --latest 5'
alias backpacmir='sudo cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.backup'
alias e='emacsclient -nw -a ""'
alias es='emacsclient -n $@'
alias paste="curl -F 'f:1=<-' ix.io"
#alias rm='echo "remove?"; ls $* ; rm -iv $*'
alias ..='cd ../'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'
#disable globbing fur curl requests (messes with url options with ? )
#alias curl='noglob curl'
#can also 'unsetopt nomatch'
#alias ns='NVIM_LISTEN_ADDRESS=/tmp/nvimsocket nvim'

#unset
unsetopt nomatch

# completions
zstyle ':completion:*' completer _oldlist _expand _complete _match _ignored _approximate
# pasting with tabs doesn't perform completion
zstyle ':completion:*' insert-tab pending
# menu if nb items > 2
zstyle ':completion:*' menu select=2
# matches case insensitive for lowercase
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'
#matches all case insensitivity
#zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'
#prompt
#PROMPT=' %F{magenta}%B%.%b%f %F{cyan}λ%f '
#PROMPT=' %F{magenta}%B%.%b%f %F{cyan}φ%f '
#PROMPT=' %F{magenta}%B%.%b%f %F{cyan}⇒%f '
#PROMPT=' %F{white}φ%f %F{magenta}%B%.%b%f %F{cyan}►%f '
#PROMPT=' %F{green}φ%f %F{magenta}%B%.%b%f %F{cyan}⇒%f '
#PROMPT=' %F{magenta}%B%.%b%f %F{cyan}▶%f '
#PROMPT=' %F{magenta}%B%.%b%f %F{cyan}►%f '

add-zsh-hook precmd vcs_info
PROMPT=' %F{magenta}%B%.%b%f %F{cyan}>%f '
RPROMPT='%F{yellow}${vcs_info_msg_0_}%f $?'
#makes commands typed bold
zle_highlight=(default:bold)

# Enable checking for (un)staged changes, enabling use of %u and %c
zstyle ':vcs_info:*' check-for-changes true
# Set custom strings for an unstaged vcs repo changes (*) and staged changes (+)
zstyle ':vcs_info:*' unstagedstr ' ~'
zstyle ':vcs_info:*' stagedstr ' +'
# Set the format of the Git information for vcs_info
zstyle ':vcs_info:git:*' formats       '(%b%u%c)'
zstyle ':vcs_info:git:*' actionformats '(%b|%a%u%c)'


# needed to tell emacs what PWD is so that it can open files relative to that
if [ -n "$INSIDE_EMACS" ]; then
    chpwd() {
        print -P "\032/$(pwd)"
    }
fi

# commands to replace basic commands when present
if type rg &> /dev/null; then
    export FZF_DEFAULT_COMMAND='rg --files'
    export FZF_DEFAULT_OPTS='-m --height 50% --border'
fi

if type exa &> /dev/null; then
    alias ls='exa'
    alias la='exa -a'
    alias ll='exa -lbh'
fi

if type bat &> /dev/null; then
    alias cat='bat --theme=gruvbox-dark --style="changes,plain"'
fi

if type rg &> /dev/null; then
    alias grep='rg'
fi

source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
